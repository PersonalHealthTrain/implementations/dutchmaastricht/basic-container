import os
import json

endpointType = "SPARQL"
if os.environ.get("endpointType") != None:
    endpointType = os.environ.get("endpointType")

endpointUrl =  "http://sparql.cancerdata.org/namespaces/kb/sparql"
if os.environ.get("endpointUrl") != None:
    endpointUrl = os.environ.get("endpointUrl")

##### if you want to use the input configuration (sent when running/requesting to run a container), enable these lines
# with open("/input.txt") as f:
#     inputArgs = json.load(f)

#############################################
#do whatever you want from here
#############################################
print("logging output")

outputJson = {"endpointType": endpointType, "endpointUrl": endpointUrl }

# Write output to file
with open('output.txt', 'w') as f:
    f.write(json.dumps(outputJson))